/**
 * Created by andr on 05.08.16.
 */
"use strict";


let
  mqtt = require('mqtt'),
  client = mqtt.connect('mqtt://127.0.0.1:1883'),
  Keeper = require('./keeper'),

  keeper = new Keeper(client);


client.on('connect', () => {
  console.log('Connected to MQTT-broker, registering keeper');
  keeper.connect();
  keeper.subscribe();
});


client.on('message', (channel, message) => {
  keeper.processMessage(channel, message);
});

client.on('error', err => console.log(err));

/*

// Temperature will be trying to reach slider value (just for fun)
let
  temperature = 23; // base temperature

setInterval(() => {
  let
    sliderValue = device.getSensorValues('mainTemperature').value;

  temperature = sliderValue > temperature ? temperature + 1 : temperature - 1;

  device.setSensorValues('tSensor', {value: temperature});
}, 3500);*/
