/**
 * Created by andr on 06.08.16.
 */
"use strict";

class Keeper {
  constructor(client) {
    this.client = client;
    this.name = 'Keeper';
    this.description = 'Scripts Keeper';
    this.scripts = [];
  }

  addScript(script) {
    this.scripts.push(script);
  }

/*  setSensorValues(name, values) {
    if (this.scripts[name]) {
      this.scripts[name].values = values;
      this.sendSensorValues(name);
    }
  }*/

/*  getSensorValues(name) {
    return this.scripts[name] && this.scripts[name].values;
  }*/


/*  sendSensorValues(name) {
    this.client.publish(`/outbox/${this.name}/${name}`,
      JSON.stringify(this.getSensorValues(name)));
  }*/

  get info() {
    return JSON.stringify({
      "deviceInfo": {
        "name": this.name,
        "endPoints": this.scripts.map(script => script.widget),
        "description": this.description,
        "status": this.status
      }
    });
  }

  get status() {
    return "good";
  }

  publishDeviceInfo() {
    this.client.publish(`/outbox/${this.name}/deviceInfo`, this.info);
  }

  lwt() {
    this.client.publish(`/outbox/${this.name}/lwt`, 'nothing');
  }

  connect() {
    this.publishDeviceInfo();
    this.lwt();
  }

  subscribe() {
    this.client.subscribe(`/inbox/${this.name}/deviceInfo`);
    this.client.subscribe('#');
  }

  processMessage(url, message) {
    console.log('Process message', url, message.toString());
    if (url === '/inbox/Keeper/deviceInfo') {
      this.publishDeviceInfo();
    }
  }

}

module.exports = Keeper;