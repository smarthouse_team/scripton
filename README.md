**Scripton**
======================

Scripton is a service written in addition to the
 [Crouton](http://crouton.mybluemix.net/crouton/gettingStarted) dashboard.
Scripton can run user's scripts that react on MQTT events. It connects to **MQTT broker**
and listens to all messages to analyze witch script it has to run.

Running the application:
--------------
```
    $ git clone git@bitbucket.org:Hastler/scripton.git
    $ cd scripton
    $ npm install
    $ npm start
```

Getting Started
-------
To start you need a **MQTT Broker** and **Crouton Dashboard**.