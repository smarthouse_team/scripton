/**
 * Created by andr on 06.08.16.
 */
'use strict';

class Sensor {
  constructor(sensor) {
    this.name = sensor.name;
    this.title = sensor.title;
    this.units = sensor.units;
    this.canReceiveValues = false;
  }

  set values(values) {
    this._values = values;
  }

  get values() {
    return this._values;
  }
}

module.exports = Sensor;